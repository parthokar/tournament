@extends('tournament.layout.master')

@section('content')
  <h1 class="text-center">Article List</h1>

  <table class="table table-bordered">
      <tr>
          <th>id</th>
          <th>Name</th>
          <th>Description</th>
          <th>Url</th>
          <th>Category</th>
          <th>Language</th>
          <th>Country</th>
      </tr>

      @forelse ($list as $item)
      <tr>
          <td>{{$item['id']}}</td>
          <td>{{$item['name']}}</td>
          <td>{{$item['description']}}</td>
          <td>{{$item['url']}}</td>
          <td>{{$item['category']}}</td>
          <td>{{$item['language']}}</td>
          <td>{{$item['country']}}</td>
      </tr>
      @empty
      <p>No Data Found</p>
      @endforelse

  </table>
@endsection 