@extends('tournament.layout.master')

@section('content')
  <h1 class="text-center">Share your idea with us</h1>
  <div class="container">
     @include('tournament.include.message')
    <form method="post" action="{{route('idea.store')}}">
      @csrf   
    <div class="form-group">
       <label>Name (required)</label>
       <input type="text" class="form-control" name="name" placeholder="Enter your name" value="{{old('name')}}">
       @if($errors->has('name'))
         <span class="text-danger"> {{$errors->first('name')}}</span>
       @endif
    </div>
    
    <div class="form-group">
        <label>Email (required)</label>
        <input type="email" class="form-control" name="email" placeholder="Enter your email" value="{{old('email')}}">
        @if($errors->has('email'))
         <span class="text-danger"> {{$errors->first('email')}}</span>
       @endif
    </div>

    <div class="form-group">
        <label>Idea (required)</label>
        <textarea  name="idea" class="form-control" placeholder="Enter your idea" value="{{old('idea')}}"></textarea>
        @if($errors->has('idea'))
         <span class="text-danger"> {{$errors->first('idea')}}</span>
       @endif
    </div>

    <button type="submit" class="btn btn-success">Create</button>
  </form>
  </div>
@endsection 