
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="{{route('home.page')}}">Logo</a>
    
    <!-- Links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{route('home.page')}}">Home</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{route('idea.create')}}">Share Idea</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{route('article.list')}}">Article List</a>
      </li>

    </ul>
  </nav>