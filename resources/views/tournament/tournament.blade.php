@extends('tournament.layout.master')

@section('content')
  <h1 class="text-center">Welcome</h1>
  <div class="container">
     @include('tournament.include.message')
    
     Tournament: {{$tournament->tournament_name}}

     <p>Note: This system is automatic if you win the tournament a confirmation mail is send to your email</p>
  </div>
@endsection 