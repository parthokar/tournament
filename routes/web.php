<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\tournament\HomeController;
use App\Http\Controllers\tournament\IdeaController;
use App\Http\Controllers\tournament\TournamentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//home route start
Route::get('/', [HomeController::class, 'index'])->name('home.page');

//idea route start
Route::get('/idea/create', [IdeaController::class, 'index'])->name('idea.create');
Route::post('/idea/store', [IdeaController::class, 'store'])->name('idea.store');

//tournament route start
Route::get('/tournament', [TournamentController::class, 'index'])->name('tournament.page');

//article route start
Route::get('/article', [HomeController::class, 'articleList'])->name('article.list');