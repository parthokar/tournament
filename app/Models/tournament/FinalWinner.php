<?php

namespace App\Models\tournament;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class FinalWinner extends Model
{
    //this function show user details
    public function user(){
      return $this->belongsTo(User::class,'user_id');
    }
}
