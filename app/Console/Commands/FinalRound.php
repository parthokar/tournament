<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\tournament\FinalWinner;
use App\Models\tournament\FirstWinner;
use App\Models\tournament\SecondWinner;
use App\Models\User;
use App\Models\tournament\Tournament;
use Mail;

class FinalRound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'final:round';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for final round';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = SecondWinner::where('status',0)->get()->random(1);

        $countFinal=FinalWinner::count();
        if($countFinal>0){
          FinalWinner::truncate();
        }
        foreach($users as $user){
          $store = new FinalWinner;
          $store->user_id=$user->user_id;
          $store->status=1;
          $store->save();
        }

        //send congratulation mail to final round user
         $finalWinner = FinalWinner::first();
         $tournamentName = Tournament::first();
         if(isset($tournamentName)){
           $tournaments=$tournamentName->tournament_name;
         }else{
            $tournaments="Tournament".rand();
         }
         $email=$finalWinner->user->email;
         $key='';
         $data='Congratulations! you are winner of'.$tournaments;

         Mail::raw("{$key} -> {$data}", function ($mail) use ($email) {
                $mail->from('tournament@2021.com');
                $mail->to($email)
                    ->subject('Tournament Winner'.date('Y'));
         });


         //send rejected mail to all user except final winner
          $restUser=User::where('id','!=',$finalWinner->user_id)->get();
          foreach($restUser as $user){
            $email=$user->email;  
            $key='';
            $data='Sorry you are not winner of'.$tournaments; 
                Mail::raw("{$key} -> {$data}", function ($mail) use ($email) {
                $mail->from('tournament@2021.com');
                $mail->to($email)
                    ->subject('Tournament Result'.date('Y'));
             });
          }

          //delete table data from table
          User::truncate();
          Tournament::truncate();
          FirstWinner::truncate();
          SecondWinner::truncate();
          FinalWinner::truncate();


    }
}
