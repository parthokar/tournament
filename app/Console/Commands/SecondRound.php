<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\tournament\FirstWinner;
use App\Models\tournament\SecondWinner;

class SecondRound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'second:round';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for second round';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = FirstWinner::where('status',0)->get()->random(2);
        $countSecond=SecondWinner::count();
        if($countSecond>0){
            SecondWinner::truncate();
        }
        foreach($users as $user){
          $store = new SecondWinner;
          $store->user_id=$user->user_id;
          $store->status=0;
          $store->save();
        }
    }
}
