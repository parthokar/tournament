<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\User;
use App\Models\tournament\FirstWinner;

class FirstRound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'first:round';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for first round';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('status',0)->get()->random(4);

        $countFirst=FirstWinner::count();
        if($countFirst>0){
            FirstWinner::truncate();
        }
        foreach($users as $user){
          $store = new FirstWinner;
          $store->user_id=$user->id;
          $store->status=0;
          $store->save();
        }
    }
}
