<?php

namespace App\Http\Requests\tournament;

use Illuminate\Foundation\Http\FormRequest;

class IdeaValidationRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'idea' => ['required', 'string', 'idea', 'max:255'44444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444],
        ];
    }
}
