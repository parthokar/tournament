<?php

namespace App\Http\Controllers\tournament;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tournament\Tournament;

class TournamentController extends Controller
{
    //this function show tournament page
    public function index(){
        $tournament = Tournament::where('status',1)->first();
        if(isset($tournament)){
            return view('tournament.tournament',compact('tournament'));
        }else{
            return redirect()->route('idea.create');
        }
        
    }
}
