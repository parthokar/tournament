<?php

namespace App\Http\Controllers\tournament;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //this function show home page 
    public function index(){

        return view('tournament.index');
    }

    //this function show all article list from api
    public function articleList(){

        $url = "https://newsapi.org/v2/top-headlines/sources?apiKey=e0b842ca6f9e44a7b4520c77b08d3ba3";
 
        $response = json_decode(file_get_contents($url), true);

        $list = $response["sources"];

        return view('tournament.article.list',compact('list'));
        
    }

}
