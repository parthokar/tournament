<?php

namespace App\Http\Controllers\tournament;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\tournament\IdeaValidationRequest;
use App\Models\User;
use App\Models\tournament\Tournament;

class IdeaController extends Controller
{
     //this function show idea create page 
     public function index(){
        return view('tournament.idea.create');
     }

     //this function store user idea
     public function store(IdeaValidationRequest $request){

        //check if already exists tournament
        //then user show a error message
        $count = Tournament::where('status',1)->count();
        if($count>0){
            return redirect()->route('tournament.page')->with('error','A tournament has already been created please try after sometime');
        }

        //check 8 idea has been complete to create tournament
        $checkIdea = User::where('status',0)->count();
        if($checkIdea==8){
            $tournament = new Tournament;
            $tournament->tournament_name = "Tournament".rand();
            $tournament->status = 1;
            $tournament->save();
            return redirect()->route('tournament.page')->with('success','Tournament has been created');
        }

        $idea = new User;
        $idea->name   = $request->name;
        $idea->email  = $request->email;
        $idea->idea   = $request->idea;
        $idea->status = 0;
        $idea->save();
        return back()->with('success','Idea has been created successfully');
     }
}
